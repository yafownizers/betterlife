from django.urls import path, include
from django.contrib.auth import views

from . import views
from django.conf import settings

urlpatterns = [
	path('', views.about, name='about')
]
