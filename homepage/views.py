from django.shortcuts import render
from django.http import HttpResponse
import os, json
from django.conf import settings
from helper.views import *
from pprint import pprint
# Create your views here.

def index(request):
    json_path = os.path.join(settings.BASE_DIR, 'static', "data/cities_with_data.json")
    cities = json.load(open(json_path))
    cities=cities.keys()
    name = request.GET.get('unadvanced')
    return render(request, 'index.html',{'cities':json.dumps(list(cities))})

def get_best_cities(request):
    current_city = request.GET.get('city', '')
    pd_val = int(request.GET.get('pd', 2))
    aqi_val = int(request.GET.get('aqi', 2))
    lc_val = int(request.GET.get('lc', 2))
    dist_val = 2
    preference_list=[pd_val,aqi_val,lc_val,dist_val]
    print("preference_list: ",preference_list)

    best_cities=get_recommended_cities(current_city,preference_list)
    pprint(best_cities)
    jumlah = len(best_cities)
    runnerUp= []
    for i in range(1,jumlah):
        runnerUp.append(best_cities[i])

    return render(request, 'result.html',{'best_cities':best_cities, 'runnerUp':runnerUp})

def get_recommended_cities(current_city,preference_list):
    json_path = os.path.join(settings.BASE_DIR, 'static', "data/cities_with_data.json")
    cities = json.load(open(json_path))
    # make array of all city coordinates
    city_coordinates = []
    for city, dat in cities.items():
        city_coordinates.append([dat['coordinates'][0], dat['coordinates'][1]])
    result = n_options_city_to_live(current_city, 30, cities, city_coordinates,preference_list)
    return result
    
def cities_list(request):
    json_path = os.path.join(settings.BASE_DIR, 'static', "data/cities_with_data.json")
    cities = json.load(open(json_path))
    return render(request, 'cities-list.html', {'cities': cities})