from django.urls import path, include
from django.contrib.auth import views

from . import views
from django.conf import settings

urlpatterns = [
	path('', views.index, name='index'),
	path ('best_city/', views.get_best_cities, name='best_city'),
	path('cities-list/', views.cities_list, name='cities-list')
]
