#!/usr/bin/env python
# coding: utf-8

# In[2]:


import json
from pprint import pprint

with open('data.json') as f:
    cities = json.load(f)
    
cities_with_aqi_gt_40=[]

#make array of all city coordinates
city_coordinates=[]
for city,dat in cities.items():
    city_coordinates.append([dat['coordinates'][0],dat['coordinates'][1]])
    if dat['aqi']>40:
        cities_with_aqi_gt_40.append(city)
    
print(cities_with_aqi_gt_40)


# In[3]:


import numpy as np
import matplotlib
from matplotlib import pyplot as plt
from matplotlib import figure

#visualize all cities in the domain
data = np.array(city_coordinates)
x, y = data.T
plt.scatter(x,y)
fig = matplotlib.pyplot.gcf()
fig.set_size_inches(20, 15)
# fig.savefig('test2png.png', dpi=300)
plt.show()


# In[5]:


import math

def get_frontiers(current_city_coordinates):
    """
    get_frontiers: return c
    input: current_city_coordinates (x,y)
    output: city as object that has name, aqi_val, hdi_val, and coordinates
    """
    #meng-cut agar frontier hanyalah radius 1 satuan koordinat
    frontier_coordinates=[]
    for city in city_coordinates:
        if math.sqrt((current_city_coordinates[0]-city[0])**2 +                 (current_city_coordinates[1]-city[1])**2) <= 1 :
            frontier_coordinates.append(city)

    #make neighbors as object
    frontier_cities={}
    for city,dat in cities.items():
        for frontier_coordinate in frontier_coordinates:
            if frontier_coordinate == dat['coordinates']:
                frontier_cities[city] = dat
    
#     #visualisasi frontier
#     data = np.array(frontier_coordinates)
#     x, y = data.T
#     fig, ax = plt.subplots()
#     ax.scatter(x,y)
#     fig = matplotlib.pyplot.gcf()
#     fig.set_size_inches(15.5, 10)
#     # fig.savefig('test2png.png', dpi=300)
    
#     #visualize city names as label
#     for i, city_name in enumerate(frontier_cities):
#         ax.annotate(city_name, (x[i], y[i]))
        
    return frontier_cities


# In[ ]:


def convert_hdi(hdi_val):
    """
    convert HDI value to 0-1 scale
    """
    Max = 0.953 #(Norway)
    Min = 0.354 #(Niger)
    return hdi_val / (Max-Min)


def convert_aqi(aqi_val):
    """
    convert AQI value to 0-1 scale
    """
#     if aqi_val < 200:
#         return 1 - (aqi_val * 0.0035)
#     else:
#         return 0.3 - ((aqi_val-200) * 0.001)

    return 1-(aqi_val*0.005)
def convert_distance(xy_coordinate,zero_point):
    """
    convert AQI value to 0-1 scale
    """
    temp_distance = math.sqrt((zero_point[0]-xy_coordinate[0])**2 + (zero_point[1]-xy_coordinate[1])**2)
    
    if temp_distance < 0.5:  #0->1 ; 0.5->0.7
        return 1 - 0.6*temp_distance
    elif temp_distance < 1.3: #0.5->0.7 ; 1.3->0.5
        return 0.7 - 0.25*(temp_distance-0.5)
    elif temp_distance < 3: #1.3->0.5 ; 3->0.38
        return 0.5 - 0.071*(temp_distance-1.3)
    else: #0.7->0.46 ; 8-> 0
        temp_distance=0.38-0.08*(temp_distance-3)
        if temp_distance<0:
            return 0
        return temp_distance

def fitness_function(aqi_converted,hdi_converted,distance_converted,aqi_scale=3,hdi_scale=3,distance_scale=3):
    """
    default fitness function (when user doesn't give any preferences)
    advanced fitness function (when user gives his/her own preferences)
    """
    return (aqi_converted*aqi_scale + hdi_converted*hdi_scale + distance_converted*distance_scale) / (aqi_scale + hdi_scale + distance_scale)

def get_city(c):
    """
    get_city: return city as a whole object
    input: city name
    output: city as object that has name, aqi_val, hdi_val, and coordinates
    """
    for city,dat in cities.items():
        if c.lower() == city.lower():
            return [city,dat]
        
def hill_climbing(current_city):
    better_cities = {}
    current_city = get_city(current_city)
    frontiers = get_frontiers(current_city[1]['coordinates'])
    zero_point= current_city[1]['coordinates']
    
    current_city_aqi = convert_aqi(current_city[1].get('aqi'))
    current_city_distance = 1
    current_city_fitness = fitness_function(current_city_aqi,1,1) ###DUMMY
#     print(current_city, current_city[1].get('aqi'), current_city_fitness)
    
    for city,val in frontiers.items():
        aqi = convert_aqi(val['aqi'])
        hdi = 1
        distance = convert_distance(val['coordinates'], zero_point)
        fit = fitness_function(aqi, hdi, distance) ###DUMMY
#         print(city,"\n AQI: ",val['aqi'])
#         print(" converted AQI: ",aqi)
#         print(" fitness val: ",fit)
#         print(" distance: ",distance)
#         print()
        # gather all better cities
        if fit>current_city_fitness:
            better_cities[city] = val
#     print(better_cities)
    if better_cities == dict():
        print("city to live:",current_city[0],current_city[1])
        return current_city
    else:
        import random
        next_city = random.choice(list(better_cities.keys()))
#         print(next_city)
        return hill_climbing(next_city)
   
        

def n_options_city_to_live(current_city,reps):
    best_cities=dict()
    for i in range(reps):
        best_city=hill_climbing(current_city)
#         print("asdsa",best_city)
        best_cities[best_city[0]]=best_city[1]
    return best_cities
        
        
# n_options_city_to_live("earley",5)
for i in cities_with_aqi_gt_40:
    print(i, " best city:")
    print(n_options_city_to_live(i,5))
    

