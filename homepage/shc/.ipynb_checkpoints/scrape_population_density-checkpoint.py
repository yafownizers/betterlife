import requests
from bs4 import BeautifulSoup
import pprint
import json

# SCRAPE WIKIPEDIA TO GET LIST OF POPULATION DENSITY
page = requests.get(
    "https://en.wikipedia.org/wiki/List_of_English_districts_by_population_density")
soup = BeautifulSoup(page.content, 'html.parser')
soup1 = soup.find(id="mw-content-text")
tables = soup1.find_all(class_="wikitable")
# for table in tables:
#     print(table.find("tr").get_text())
data = soup.select("#mw-content-text .wikitable tr")

# ADDING SCRAPPED + INDEXED CITIES INTO NEW DICTIONARY
mean_of_population_density = 0
cities_population_density = dict()
for item in list(data):
    object = item.get_text().split("\n")
    if object[2] != '':
        if "," in object[3]:
            # removing commas in the number
            object[3] = int("".join(object[3].split(",")))
        cities_population_density[object[2]] = int(object[3])
        mean_of_population_density += int(object[3])
mean_of_population_density /= len(cities_population_density)
mean_of_population_density = int(mean_of_population_density)

# pprint.pprint(cities_population_density)

# open old data
with open('data.json') as f:
    all_cities = json.load(f)

# UPDATE OLD DATA WITH POPULATION DENSITIES
for city1 in cities_population_density:
    for city2 in all_cities:
        if city1 == city2 or city1 in city2 or city2 in city1:
            all_cities[city2]["population_density"] = cities_population_density[city1]
        # if there is no matching city, set it with mean of population density
        if not all_cities[city2].get("population_density"):
            all_cities[city2]["population_density"] = mean_of_population_density
# pprint.pprint(all_cities)

# write new data
with open("cities_population_density.json", "w") as write_file:
    json.dump(all_cities, write_file)
