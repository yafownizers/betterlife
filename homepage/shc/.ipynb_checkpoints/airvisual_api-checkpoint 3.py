import requests
import json
import time
import sys
from tqdm import tqdm
import os

API = os.environ.get('API_AIRVISUAL')
STATE = "England"
COUNTRY = "UK"


def get_cities_list(state, country):
	try:
		url = "http://api.airvisual.com/v2/cities?state=" + state + "&country=" + country + "&key=" + API
		response = requests.get(url, headers={"Content-Type": "application/json", "Authorization": "Bearer 15JH0HDsW61jyZoWpLXvOKd4_X8XHU3M5lQgbcBD6NBVH0wkvbblRhNFv0n8txVq"})
		json_data = json.loads(response.text)
		if json_data["status"] == 'success':
			return [key["city"] for key in json_data["data"]]
		else:
			print(json_data["data"]["message"]+"\n")
	except requests.exceptions.Timeout:
		print("TIMEOUT... RETYRING...")
		return get_cities_list(state, country) 
	except requests.exceptions.TooManyRedirects:
		print("Wrong URL")
		sys.exit(1)
	except requests.exceptions.RequestException as e:
		print (e)
		sys.exit(1)

def get_cities_data(city, state, country):
	try:
		url = "http://api.airvisual.com/v2/city?city="+city+"&state=" + state + "&country="+ country + "&key=" + API
		response = requests.get(url, headers={"Content-Type": "application/json", "Authorization": "Bearer 15JH0HDsW61jyZoWpLXvOKd4_X8XHU3M5lQgbcBD6NBVH0wkvbblRhNFv0n8txVq"})
		json_data = json.loads(response.text)
		if json_data["status"] == 'success':
			return json_data["data"]
		else:
			print(json_data["data"]["message"]+"\n")
	except requests.exceptions.Timeout:
		print("TIMEOUT... RETYRING...")
		return get_cities_data(city, state, country)
	except requests.exceptions.TooManyRedirects:
		print("Wrong URL")
		sys.exit(1)
	except requests.exceptions.RequestException as e:
		print (e)
		sys.exit(1)

def main():
	cities_list = get_cities_list(STATE, COUNTRY)
	result = {}
	pbar = tqdm(total=len(cities_list))
	counter = 0
	while (counter < len(cities_list)):
		city = (cities_list[counter])
		city_data = get_cities_data(city, STATE, COUNTRY)
		try:
			result[city] = {"aqi": city_data["current"]["pollution"]["aqius"], "coordinates": city_data["location"]["coordinates"]}
		except:
			continue
		print(city + "is added")
		pbar.update(1) 
		counter += 1
	pbar.close()
	with open('data.json', 'w', encoding='utf-8') as f:
		json.dump(result, f, ensure_ascii=False, indent=4)
    
main()
