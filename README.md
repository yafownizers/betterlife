## BETTERLIFE

BETTERLIFE merupakan sebuah proyek berbasis web-application yang dapat memberikan user rekomendasi kota terdekat yang mempunyai kualitas lebih baik dibanding kota sebelumnya berdasarkan variabel-variabel prioritas setiap user. Beberapa contoh dari variabel prioritas tersebut adalah HDI (Human Development Index) untuk kesejahteraan, AQI (Air Quality Index) untuk kesehatan lingkungan, dan budget untuk kepentingan ekonomi user. Aplikasi BetterLife akan membantu user untuk mewujudkan lingkungan yang user butuhkan dalam rangka menjaga kesehatan mental dan psikologisnya. Proyek ini dibuat untuk memenuhi proyek akhir kelas Sistem Cerdas.

**Made by:** 
- Khalis Murfid | 1706040164 
- Muhammad Anwar Farihin | 1706039635 
- Muhammad Irfan Amrullah | 1706039585 
- Yafonia Kristiani | 1706039521 

- [link proposal](https://docs.google.com/document/d/1-oH4OAm_eJzSioQwGrc1D1q2AkanxLz-z0kGn5GJZ9s/edit?usp=sharing)

