from django.urls import path, include
from django.contrib.auth import views

from . import views
from django.conf import settings

urlpatterns = [
    path ('cities/', views.get_recommended_cities, name='get_recommended_cities'),
    path('list-cities/', views.get_list_cities, name='get_list_cities')
]
