import json
import math
import os
import random
from django.http import HttpResponse
from django.conf import settings

def get_recommended_cities(request):
    json_path = os.path.join(settings.BASE_DIR, 'static', "data/cities_with_data.json")
    cities = json.load(open(json_path))
    current_city = request.GET.get('city', '')
    pd_val = int(request.GET.get('pd_val', 2))
    aqi_val = int(request.GET.get('aqi_val', 2))
    lc_val = int(request.GET.get('lc_val', 2))
    dist_val = int(request.GET.get('dist_val', 2))
    
    # make array of all city coordinates
    city_coordinates = []
    for city, dat in cities.items():
        city_coordinates.append([dat['coordinates'][0], dat['coordinates'][1]])
    result = n_options_city_to_live(current_city, 20, cities, city_coordinates,[pd_val,aqi_val,lc_val,dist_val])
    return HttpResponse(
        json.dumps(result),
        content_type='application/javascript; charset=utf8'
    )

def get_frontiers(current_city_coordinates, city_coordinates, cities):
    """
    get_frontiers: return c
    input: current_city_coordinates (x,y)
    output: city as object that has name, aqi_val, hdi_val, and coordinates
    """
    # meng-cut agar frontier hanyalah radius 1 satuan koordinat
    frontier_coordinates = []
    for city in city_coordinates:
        if math.sqrt((current_city_coordinates[0] - city[0]) ** 2 + (current_city_coordinates[1] - city[1]) ** 2) <= 1:
            frontier_coordinates.append(city)

    # make neighbors as object
    frontier_cities = {}
    for city, dat in cities.items():
        for frontier_coordinate in frontier_coordinates:
            if frontier_coordinate == dat['coordinates']:
                frontier_cities[city] = dat
    return frontier_cities

def logistic_function(value,k):
    """
    value = number to be mapped,
    e = the natural logarithm base (also known as Euler's number),
    k = the logistic growth rate or steepness of the curve.[1]
    """
    return 1/(1+math.e**(-value*k))

def convert_population_density(pd_val):
    """
    convert population density value to 0-1 scale
    Max = 14681 #(Hackney)
    Min = 104 #(Carlisle)
    """
    return (1-logistic_function(pd_val,0.00025))*2

def convert_living_cost(lc_val):
    """
    convert living cost value to 0-1 scale
    Max = 99989 #(Preston)
    Min = 10287 #(Henley-on-Thames)
    """
    return (1-logistic_function(lc_val,0.00002))*2

def convert_aqi(aqi_val):
    """
    convert AQI value to 0-1 scale
    max aqi: Devizes 84
    min aqi: Hartlepool 0
    """
    return  (1-logistic_function(aqi_val,0.01))*2

def eucladian_dist(xy_coordinate, zero_point):
    return math.sqrt(((zero_point[0] - xy_coordinate[0])*111.2) ** 2 + ((zero_point[1] - xy_coordinate[1])*111.2) ** 2)

def convert_distance(xy_coordinate, zero_point):
    
    """
    convert distance value to 0-1 scale
    """
    temp_distance = eucladian_dist(xy_coordinate, zero_point)
    return (1-logistic_function(temp_distance,0.001))*2

def fitness_function(pd_converted, aqi_converted, lc_converted,dist_converted,preference_list):
    """
    default fitness function (when user doesn't give any preferences)
    advanced fitness function (when user gives his/her own preferences)
    
    factors that affect the fitness function:
        - population density
        - air quality index
        - living cost
        - distance from the old city
    """
    return (pd_converted*preference_list[0] + aqi_converted*preference_list[1] + lc_converted*preference_list[2] + dist_converted*preference_list[3]) / sum(preference_list)
 

def get_city(city, cities):
    """
    get_city: return city as a whole object
    input: city name
    output: city as a whole object 
    """
    try: 
        return [city, cities[city]]
    except KeyError:
        return []

def hill_climbing(current_city, cities, city_coordinates,preference_list,zero_point):
    """
    stochastic hill climbing,
    randomly return a city that is better than the old one
    """
    better_cities = {}
    current_city = get_city(current_city, cities)
    frontiers = get_frontiers(current_city[1]['coordinates'], city_coordinates, cities)

    current_city_density = convert_population_density(current_city[1].get('population_density'))
    current_city_aqi = convert_aqi(current_city[1].get('aqi'))
    current_city_living_cost = convert_living_cost(current_city[1].get('living_cost'))
    current_city_distance = convert_distance(current_city[1]['coordinates'], zero_point)
    current_city_fitness = fitness_function(current_city_density,current_city_aqi,current_city_living_cost,current_city_distance,preference_list)
    
    
    current_city[1]["converted_distance"]=current_city_distance
    current_city[1]["converted_aqi"]=current_city_aqi
    current_city[1]["converted_living_cost"]=current_city_living_cost
    current_city[1]["converted_distance"]=current_city_distance
    current_city[1]["converted_fitness"]=current_city_fitness
    current_city[1]["distance"]="%.2f" %eucladian_dist(current_city[1]['coordinates'], zero_point)
    #     print(current_city, current_city[1].get('aqi'), current_city_fitness)

    for city, val in frontiers.items():
        city_density = convert_population_density(val.get('population_density'))
        city_aqi = convert_aqi(val.get('aqi'))
        city_living_cost = convert_living_cost(val.get('living_cost'))
        city_distance = convert_distance(val['coordinates'], zero_point)
        city_fitness = fitness_function(city_density,city_aqi,city_living_cost,city_distance,preference_list)
    
#         print(city)
#         print(" city_density: ",city_density)
#         print(" city_aqi: ",city_aqi)
#         print(" city_living_cost: ",city_living_cost)
#         print(" city_distance: ",city_distance)
#         print(" city_fitness: ",city_fitness)
#         print()
        # gather all better cities
        if city_fitness > current_city_fitness:
            better_cities[city] = val
            current_city[1]["converted_population_density"]=city_density
            current_city[1]["converted_aqi"]=city_aqi
            current_city[1]["converted_living_cost"]=city_living_cost
            current_city[1]["converted_distance"]=city_distance
            current_city[1]["converted_fitness"]=city_fitness
            current_city[1]["distance"]="%.2f" %eucladian_dist(val['coordinates'], zero_point)
    #     print(better_cities)
    if better_cities == dict():
        # print("city to live:", current_city[0], current_city[1],"\n\n")
        return current_city
    else:
        next_city = random.choice(list(better_cities.keys()))
        #         print(next_city)
        return hill_climbing(next_city, cities, city_coordinates,preference_list,zero_point)


def n_options_city_to_live(current_city, reps, cities, city_coordinates,preference_list):
    best_cities = dict()
    current_city_coordinates = get_city(current_city,cities)[1]['coordinates']
    for _ in range(reps):
        best_city = hill_climbing(current_city, cities, city_coordinates,preference_list,current_city_coordinates)
        #         print("asdsa",best_city)
        best_cities[best_city[0]]= best_city[1]
    return list(best_cities.items())

def get_list_cities(request):
    json_path = os.path.join(settings.BASE_DIR, 'static', "data/cities_with_data.json")
    cities = json.load(open(json_path))
    return HttpResponse(
        json.dumps(cities),
        content_type='application/javascript; charset=utf8'
    )



